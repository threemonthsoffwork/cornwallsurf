from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
import time

class NewVisitorTest(LiveServerTestCase):

	def setUp(self):
		self.browser = webdriver.Firefox()

	def tearDown(self):
		self.browser.quit()

	def test_can_access_webpage(self):
		# A user visits the website and notices the header mentions KSurf
		self.browser.get(self.live_server_url)	
		self.assertIn('KSurf', self.browser.title)
